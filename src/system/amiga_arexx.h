/*-
 * Copyright (c) 2014-2021 Carsten Sonne Larsen <cs@innolan.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project homepage:
 * https://amath.innolan.net
 *
 */

#ifndef AMATH_AREXX
#define AMATH_AREXX

/**
 * @file  console.h
 * @brief Generic console system calls.
 *
 */

#if defined(AMIGA)

#ifndef AREXXPORTNAME
#define AREXXPORTNAME "AMATH."
#endif

#ifndef AREXXPROCNAME
#define AREXXPROCNAME "amath ARexx process"
#endif

/**
 * @brief Abstract base class encapsulating console logic.
 *
 */
class AmigaARexx
{
public:
    AmigaARexx();
    ~AmigaARexx();
    bool Start();
    void Stop();

private:
    static void ProcessEntry();
    void Process();
    void CreatePort();
    void PublishPort();
    void DestroyPort();
    bool HandleMessage(struct RexxMsg *message);

    char portName[10];
    struct Process *process;
    struct MsgPort *port;
};

#endif
#endif
