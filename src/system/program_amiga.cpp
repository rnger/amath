/*-
 * Copyright (c) 2014-2021 Carsten Sonne Larsen <cs@innolan.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project homepage:
 * https://amath.innolan.net
 *
 */

#include "amath.h"
#include "amathc.h"
#include "console.h"
#include "filesystem.h"
#include "program_amiga.h"
#include "console_amiga.h"
#include "window_amiga.h"
#include "language_amiga.h"
#include "filesystem_amiga.h"
#include "preferences_amiga.h"
#include "lib/charbuf.h"
#include "main/evaluator.h"
#include "amiga_arexx.h"

#if defined(AMIGA)

#include <clib/exec_protos.h>
#include <clib/dos_protos.h>

#ifndef DOSNAME
#define DOSNAME "dos.library"
#endif
#define DOSREV 33L

#ifndef INTUITIONNAME
#define INTUITIONNAME "intuition.library"
#endif
#define INTUITIONREV 34L

#ifndef GRAPHICSNAME
#define GRAPHICSNAME "graphics.library"
#endif
#define GRAPHICSREV 34L

#ifndef LOCALENAME
#define LOCALENAME "locale.library"
#endif
#define LOCALEREV 34L

static struct DosBase *DosBase = nullptr;
static struct GfxBase *GfxBase = nullptr;
static struct LocaleBase *LocaleBase = nullptr;
static struct IntuitionBase *IntuitionBase = nullptr;

#define ARGS_FORMAT "SHELL/S,NOANSI/S,AREXX/S,INPUT/F"

#if defined(AROS)
#define RDPTR IPTR *
#else
#define RDPTR LONG *
#endif

AmigaProgram::AmigaProgram()
    : Program()
{
    DosBase = (struct DosBase *)OpenLibrary(DOSNAME, DOSREV);
    IntuitionBase = (struct IntuitionBase *)OpenLibrary(INTUITIONNAME, INTUITIONREV);
    GfxBase = (struct GfxBase *)OpenLibrary(GRAPHICSNAME, GRAPHICSREV);
    LocaleBase = (struct LocaleBase *)OpenLibrary(LOCALENAME, LOCALEREV);

    rdargs = nullptr;
    args.shell = FALSE;
    args.noansi = FALSE;
    args.arexx = FALSE;
    args.input = nullptr;
    Console = nullptr;
}

AmigaProgram::~AmigaProgram()
{
    if (Console != nullptr)
    {
        Console->Close();
        delete Console;
    }

    if (rdargs != nullptr)
    {
        FreeArgs(rdargs);
    }

    if (DosBase != nullptr)
        CloseLibrary((struct Library *)DosBase);

    if (LocaleBase != nullptr)
        CloseLibrary((struct Library *)LocaleBase);

    if (GfxBase != nullptr)
        CloseLibrary((struct Library *)GfxBase);

    if (IntuitionBase != nullptr)
        CloseLibrary((struct Library *)IntuitionBase);
}

void AmigaProgram::Initialize(int argc, char **argv)
{
    if (DosBase == NULL)
        return;

    if (IntuitionBase == NULL)
        return;

    if (GfxBase == NULL)
        return;

    if (LocaleBase == NULL)
        return;

    rdargs = (RDArgs *)ReadArgs((const char *)ARGS_FORMAT, (RDPTR)&args, 0);
    if (!rdargs)
    {
        PrintFault(IoErr(), (STRPTR)argv[0]);
        return;
    }

    shellMode = args.shell ? true : false;
    ansiMode = args.noansi ? false : true;

    if (shellMode || args.input != nullptr)
    {
        Console = new AmigaShellConsole(Preferences->GetPrompt());
    }
    else
    {
        Console = new AmigaWindow(Preferences->GetPrompt(), Language);
    }

    InitAnsiMode();
}

void AmigaProgram::Start()
{
    if (Console == nullptr || !Console->Open())
    {
        return;
    }

    Preferences->Load();

    if (args.input != nullptr)
    {
        Evaluator *evaluator = new Evaluator(args.input);
        evaluator->Evaluate();
        const char *res = evaluator->GetResult();
        Console->WriteString(res);
        Console->ResetConsole();
        delete evaluator;
        return;
    }

    AmigaARexx *arexx = new AmigaARexx();
    arexx->Start();

    if (args.arexx)
    {
        Wait(SIGBREAKF_CTRL_C);
    }
    else
    {
        Console->Start();
    }

    arexx->Stop();
    delete arexx;

    return;
}

#endif
